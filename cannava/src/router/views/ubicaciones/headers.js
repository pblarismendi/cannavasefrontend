const predio = [
    {
        value: "codigo",
        text: "Identificador",
        sortable: true,
    },
    {
        value: "nombre",
        text: "Nombre",
        sortable: true,
    },
    {
        value: "tipo",
        text: "Tipo",
        sortable: true,
    },
    {
        value: "direccion",
        text: "Dirección",
        sortable: true,
    },
    {
        value: "observaciones",
        text: "Observaciones",
        sortable: true,
    },
    {
        value: "actions",
        text: "Acciones",
        sortable: false,
    }
];

const instalacion = [
    {
        value: "codigo",
        text: "Identificador",
        sortable: true,
    },
    {
        value: "nombre",
        text: "Nombre",
        sortable: true,
    },
    {
        value: "tipo",
        text: "Tipo",
        sortable: true,
    },
    {
        value: "predio",
        text: "Predio",
        sortable: true,
    },
    {
        value: "observaciones",
        text: "Observaciones",
        sortable: true,
    },
    {
        value: "",
        text: "Acciones",
        sortable: true,
    }

];

const division = [
    {
        value: "codigo",
        text: "Identificador",
        sortable: true,
    },
    {
        value: "nombre",
        text: "Nombre",
        sortable: true,
    },
    {
        value: "tipo",
        text: "Tipo",
        sortable: true,
    },
    {
        value: "intalacion",
        text: "Instalacion",
        sortable: true,
    },
    {
        value: "observaciones",
        text: "Observaciones",
        sortable: true,
    },
    {
        value: "",
        text: "Acciones",
        sortable: true,
    }

];

const almacen = [
    {
        value: "codigo",
        text: "Identificador",
        sortable: true,
    },
    {
        value: "nombre",
        text: "Nombre",
        sortable: true,
    },
    {
        value: "tipo",
        text: "Tipo",
        sortable: true,
    },
    {
        value: "division",
        text: "Division",
        sortable: true,
    },
    {
        value: "observaciones",
        text: "Observaciones",
        sortable: true,
    },
    {
        value: "",
        text: "Acciones",
        sortable: true,
    }

];

const recipiente = [
    {
        value: "codigo",
        text: "Identificador",
        sortable: true,
    },
    {
        value: "nombre",
        text: "Nombre",
        sortable: true,
    },
    {
        value: "tipo",
        text: "Tipo",
        sortable: true,
    },
    {
        value: "modelo",
        text: "Modelo",
        sortable: true,
    },
    {
        value: "marca",
        text: "Marca",
        sortable: true,
    },
    {
        value: "litros",
        text: "Litros",
        sortable: true,
    },
    {
        value: "cant",
        text: "Cantidad",
        sortable: true,
    },
    {
        value: "observaciones",
        text: "Observaciones",
        sortable: true,
    },
    {
        value: "",
        text: "Acciones",
        sortable: true,
    }

];




module.exports = {
    predio,
    instalacion,
    division,
    almacen,
    recipiente
};

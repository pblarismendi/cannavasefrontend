const route = [
    {
        path: '/ubicaciones-menu',
        name: 'ubicacionesMenu',
        component: () => import('./MenuUbicaciones.vue')
    }
];

module.exports = {
    route
};

import Home from "./views/Home";
import MenuUbicacionesRoute from "./views/ubicaciones/routes";

export default [
    {
        path: '/home',
        name: 'Home',
        component: Home
    },
    {
        path: '/404',
        name: '404',
        component: require('./views/utility/404').default,
    },
    {
        path: '*',
        redirect: '404',
    },
    ...MenuUbicacionesRoute.route
]

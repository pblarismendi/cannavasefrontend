export const menuItems = [
    {
        title: 'Inicio',
        icon: 'mdi-view-dashboard',
        link: "/"
    },
    {
        title: 'Comercial',
        icon: 'mdi-account-cash',
        link: "/404"
    },
    {
        title: 'Comunicacion',
        icon: 'mdi-newspaper-variant-multiple-outline',
        link: "/404"
    },
    {
        title: 'Inventario',
        icon: 'mdi-store',
        link: "/404"
    },
    {
        title: 'Lotes de Cultivo',
        icon: 'mdi-sprout',
        link: "/404"
    },
    {
        title: 'Operadores',
        icon: 'mdi-account-multiple',
        link: "/404"
    },
    {
        title: 'Reportes',
        icon: 'mdi-file-chart',
        link: "/404"
    },
    {
        title: 'Semillas',
        icon: 'mdi-seed',
        link: "/404"
    },
    {
        title: 'Ubicaciones',
        icon: 'mdi-map-marker',
        link: "/ubicaciones-menu"
    },
]
